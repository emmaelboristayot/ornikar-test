<?php

use App\Entity\Learner;
use App\Entity\Lesson;
use App\Entity\MeetingPoint;
use App\Entity\Template;
use App\Repository\InstructorRepository;
use Faker\Factory;

require_once __DIR__ . '/vendor/autoload.php';

$faker = Factory::create();

$start_at = $faker->dateTimeBetween("-1 month");
$end_at = $start_at->add(new DateInterval('PT1H'));

return [
    InstructorRepository::class => new InstructorRepository($faker->firstName, $faker->lastName),
    MeetingPoint::class => new MeetingPoint($faker->randomNumber(), $faker->url, $faker->city),
    Learner::class => new Learner($faker->randomNumber(), $faker->firstName, $faker->lastName, $faker->email),
    Lesson::class => new Lesson($faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber(), $start_at, $end_at),
    Template::class => new Template(
        1,
        'Votre leçon avec [lesson:instructor_name]',
        "
                    Bonjour [user:first_name],
                    
                    Merci d'avoir réservé une leçon de conduite avec  [lesson:instructor_name] le [lesson:start_date] à [lesson:meeting_point].
                    
                    Bien cordialement,
                    
                    L'équipe Ornikar
                    "),

];