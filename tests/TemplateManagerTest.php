<?php

namespace Tests;

use App\Context\ApplicationContext;
use App\Entity\Instructor;
use App\Entity\Lesson;
use App\Entity\MeetingPoint;
use App\Entity\Template;
use App\Repository\InstructorRepository;
use App\Repository\LessonRepository;
use App\Repository\MeetingPointRepository;
use App\TemplateManager;
use DateInterval;
use Generator;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \App\TemplateManager
 */
class TemplateManagerTest extends TestCase
{
    /**
     * @var ApplicationContext
     */
    private $applicationContext;
    /**
     * @var InstructorRepository
     */
    private $instructorRepository;
    /**
     * @var LessonRepository
     */
    private $lessonRepository;
    /**
     * @var MeetingPointRepository
     */
    private $meetingPointRepository;

    /**
     * @var TemplateManager
     */
    private $templateManager;

    public function setUp(): void
    {
        parent::setUp();

        $this->applicationContext = $this->createMock(ApplicationContext::class);
        $this->instructorRepository = $this->createMock(InstructorRepository::class);
        $this->lessonRepository = $this->createMock(LessonRepository::class);
        $this->meetingPointRepository = $this->createMock(MeetingPointRepository::class);

        $this->templateManager = $this->getMockBuilder(TemplateManager::class)
            ->setConstructorArgs([
                $this->applicationContext, $this->instructorRepository, $this->lessonRepository, $this->meetingPointRepository
            ])
            ->setMethods(null)
            ->getMock();
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct()
    {
        $this->assertInstanceOf(TemplateManager::class, $this->templateManager);
    }

    /**
     * @covers ::getTemplateComputed
     * @dataProvider getTemplateComputedDataProvider
     *
     * @throws \Exception
     */
    public function testGetTemplateComputed(Template $tpl, array $data, array $params, array $expected)
    {
        if (!empty($params)) {
            $this->lessonRepository
                ->method('getById')
                ->willReturn(new Lesson(
                    1,
                    $params['siteId'],
                    $params['instructorId'],
                    $params['start_at'],
                    $params['end_at']
                ));

            $this->meetingPointRepository
                ->method('getById')
                ->willReturn(new MeetingPoint(1, $params['url'], $params['name']));

            $this->instructorRepository
                ->method('getById')
                ->willReturn(new Instructor(
                    1,
                    $params['instructorFirstName'],
                    $params['instructorLastName'],
                ));
        }

        $result = $this->templateManager->getTemplateComputed($tpl, $data);

        $this->assertEquals($expected['subject'], $result->subject);
        $this->assertEquals($expected['content'], $result->content);
    }

    public function getTemplateComputedDataProvider(): Generator
    {
        $faker = \Faker\Factory::create();

        // Instructor params
        $instructorFirstName = $faker->firstName;
        $instructorLastName = $faker->lastName;

        // MeetingPoint params
        $url = $faker->url;
        $name = $faker->city;

        //Lesson
        $siteId = $faker->numberBetween(1, 10);
        $instructorId = $faker->numberBetween(1, 200);
        $start_at = $faker->dateTimeBetween("-1 month");
        $end_at = $start_at->add(new \DateInterval('PT1H'));

        yield [
            new Template(
                1,
                'Votre leçon de conduite avec [lesson:instructor_name]',
                "
                Bonjour [user:first_name],

                La reservation du [lesson:start_date] de [lesson:start_time] à [lesson:end_time] avec [lesson:instructor_name] a bien été prise en compte!
                Voici votre point de rendez-vous: [lesson:meeting_point].

                Bien cordialement,

                L'équipe Ornikar
                "),
            [
                'lesson' => null
            ],
            [],
            [
                'id' => 1,
                'subject' => 'Votre leçon de conduite avec [lesson:instructor_name]',
                "content" =>  "
                Bonjour ,

                La reservation du [lesson:start_date] de [lesson:start_time] à [lesson:end_time] avec [lesson:instructor_name] a bien été prise en compte!
                Voici votre point de rendez-vous: [lesson:meeting_point].

                Bien cordialement,

                L'équipe Ornikar
                "
            ]
        ];

        yield [
            new Template(
                1,
                'Votre leçon de conduite avec [lesson:instructor_name]',
                "Bonjour [user:first_name],La reservation du [lesson:start_date] de [lesson:start_time] à [lesson:end_time] avec [lesson:instructor_name] a bien été prise en compte!Voici votre point de rendez-vous: [lesson:meeting_point].Bien cordialement,L'équipe Ornikar"),
            [
                'lesson' => new Lesson($faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber(), $start_at, $end_at)
            ],
            [
                'instructorFirstName' => $instructorFirstName,
                'instructorLastName' => $instructorLastName,
                'url' => $url,
                'name' => $name,
                'siteId' => $siteId,
                'instructorId' => $instructorId,
                'start_at' => $start_at,
                'end_at' => $end_at,
            ],
            [
                'id' => 1,
                'subject' => 'Votre leçon de conduite avec ' . $instructorFirstName,
                "content" =>  "Bonjour ,La reservation du " . $start_at->format('d/m/Y') . " de " . $start_at->format('H:i') . " à " . $start_at->format('H:i') . " avec " . $instructorFirstName . " a bien été prise en compte!Voici votre point de rendez-vous: " . $name . ".Bien cordialement,L'équipe Ornikar"
            ]
        ];
    }
}
