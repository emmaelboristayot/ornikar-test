FROM php:7.4-apache

## Install system dependencies
RUN apt-get update \
&&  apt-get install -y --no-install-recommends wget git vim zlib1g-dev libzip-dev zip unzip \
&&  docker-php-ext-install pdo_mysql zip \
&&  rm -rf /var/lib/apt/lists/*

ADD ./ /var/www/html

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer