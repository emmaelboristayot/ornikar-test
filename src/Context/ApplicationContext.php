<?php

namespace App\Context;

use App\Entity\Learner;
use App\Entity\MeetingPoint;

class ApplicationContext
{

    /**
     * @var MeetingPoint
     */
    private $currentSite;
    /**
     * @var Learner
     */
    private $currentUser;

    public function __construct(MeetingPoint $currentSite, Learner $currentUser)
    {
        $this->currentSite = $currentSite;
        $this->currentUser = $currentUser;
    }

    public function getCurrentSite()
    {
        return $this->currentSite;
    }

    public function getCurrentUser()
    {
        return $this->currentUser;
    }
}
