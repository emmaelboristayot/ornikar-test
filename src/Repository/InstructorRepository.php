<?php

namespace App\Repository;

use App\Entity\Instructor;

class InstructorRepository implements Repository
{
    /**
     * @var string
     */
    private string $firstname;
    /**
     * @var string
     */
    private string $lastname;

    /**
     * InstructorRepository constructor.
     * @param string $firstname
     * @param string $lastname
     */
    public function __construct(string $firstname, string $lastname)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }

    /**
     * @param int $id
     *
     * @return Instructor
     */
    public function getById($id)
    {
        // DO NOT MODIFY THIS METHOD
        return new Instructor(
            $id,
            $this->firstname,
            $this->lastname
        );
    }
}
