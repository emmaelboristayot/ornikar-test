<?php

namespace App\Repository;

use App\Entity\MeetingPoint;

class MeetingPointRepository implements Repository
{
    private string $url;
    private string $name;

    /**
     * SiteRepository constructor.
     *
     */
    public function __construct()
    {
        // DO NOT MODIFY THIS METHOD
        $this->url = \Faker\Factory::create()->url;
        $this->name = \Faker\Factory::create()->city;
    }

    /**
     * @param int $id
     *
     * @return MeetingPoint
     */
    public function getById($id): MeetingPoint
    {
        // DO NOT MODIFY THIS METHOD
        return new MeetingPoint($id, $this->url, $this->name);
    }
}
