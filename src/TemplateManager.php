<?php

namespace App;

use App\Context\ApplicationContext;
use App\Entity\Learner;
use App\Entity\Lesson;
use App\Entity\Template;
use App\Repository\InstructorRepository;
use App\Repository\LessonRepository;
use App\Repository\MeetingPointRepository;
use RuntimeException;

class TemplateManager
{
    /**
     * @var ApplicationContext
     */
    private ApplicationContext $applicationContext;
    /**
     * @var InstructorRepository
     */
    private InstructorRepository $instructorRepository;
    /**
     * @var LessonRepository
     */
    private LessonRepository $lessonRepository;
    /**
     * @var MeetingPointRepository
     */
    private MeetingPointRepository $meetingPointRepository;


    /**
     * TemplateManager constructor.
     * @param ApplicationContext $applicationContext
     * @param InstructorRepository $instructorRepository
     * @param LessonRepository $lessonRepository
     * @param MeetingPointRepository $meetingPointRepository
     */
    public function __construct(ApplicationContext $applicationContext, InstructorRepository $instructorRepository, LessonRepository $lessonRepository, MeetingPointRepository $meetingPointRepository)
    {
        $this->applicationContext = $applicationContext;
        $this->instructorRepository = $instructorRepository;
        $this->lessonRepository = $lessonRepository;
        $this->meetingPointRepository = $meetingPointRepository;
    }

    /**
     * @param Template $tpl
     * @param array $data
     * @return Template
     */
    public function getTemplateComputed(Template $tpl, array $data): Template
    {
        if (!$tpl) {
            throw new RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    /**
     * @param $text
     * @param array $data
     * @return string
     */
    private function computeText($text, array $data): string
    {
        $lesson = (isset($data['lesson']) and $data['lesson'] instanceof Lesson) ? $data['lesson'] : null;
        $_user = (isset($data['user']) and ($data['user'] instanceof Learner)) ? $data['user'] : $this->applicationContext->getCurrentUser();
        $lessonLink = '';

        if ($lesson) {
            $_lessonFromRepository = $this->lessonRepository->getById($lesson->id);
            $usefulObject = $this->meetingPointRepository->getById($lesson->meetingPointId);
            $instructorOfLesson = $this->instructorRepository->getById($lesson->instructorId);

            if (strpos($text, '[lesson:instructor_link]') !== false) {
                $lessonLink = $usefulObject->url . '/' . $this->instructorRepository->getById($lesson->instructorId)->getId() . '/lesson/' . $lesson->id;
            }

            if ($lesson->meetingPointId) {
                $this->setTag('[lesson:meeting_point]', $usefulObject->name, $text);
            }

            $this
                ->setTag('[lesson:summary_html]', Lesson::renderHtml($_lessonFromRepository), $text)
                ->setTag('[lesson:summary]', Lesson::renderText($_lessonFromRepository), $text)
                ->setTag('[lesson:instructor_name]', $instructorOfLesson->getFirstname(), $text)
                ->setTag('[lesson:start_date]', $lesson->start_time->format('d/m/Y'), $text)
                ->setTag('[lesson:start_time]', $lesson->start_time->format('H:i'), $text)
                ->setTag('[lesson:end_time]', $lesson->start_time->format('H:i'), $text);
        }

        $this
            ->setTag('[lesson:link]', $lessonLink, $text)
            ->setTag('[user:first_name]', ucfirst(mb_strtolower($_user->firstname)), $text);

        return $text;
    }

    /**
     * @param string $tag
     * @param string $data
     * @param string $text
     * @return TemplateManager
     */
    private function setTag(string $tag, string $data, string &$text): self
    {
        if (strpos($text, $tag) !== false) {
            $text = str_replace($tag, $data, $text);
        }
        return $this;
    }
}
