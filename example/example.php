<?php

use App\Entity\Lesson;
use App\Entity\Template;
use App\TemplateManager;

require_once __DIR__ . '/../vendor/autoload.php';

$builder = new DI\ContainerBuilder();
$builder->useAutowiring(true);
$builder->addDefinitions('../config.php');
$container = $builder->build();

$message = $container->call([TemplateManager::class, 'getTemplateComputed'], [DI\get(Template::class), ['lesson' => $container->get(Lesson::class)]]);

echo $message->subject . "\n" . $message->content;
